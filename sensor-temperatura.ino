#include "DHT.h"

#define sensorTemperatura A1 // pino que estamos conectado
#define tipoSensor DHT11 // Define o tipo de sensor
int ledAlerta = 3;

DHT dht(sensorTemperatura, tipoSensor);

void setup() {
  Serial.begin(9600);
  dht.begin();
}

void loop() {
  float temperatura = dht.readTemperature();
  if (isnan(temperatura)) {
    Serial.println("Não foi possível ler a temperatura");
  } else {
    Serial.print("Temperatura: ");
    Serial.print(temperatura);
    Serial.println(" C");
    delay(1000);
  }
}
